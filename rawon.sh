#!/bin/bash
# Copyright (c) 2018, salesforce.com, inc.
# All rights reserved.
# SPDX-License-Identifier: BSD-3-Clause
# For full license text, see the LICENSE file in the repo root or https://opensource.org/licenses/BSD-3-Clause

set -e

usage() {
cat << EOF
Rawon as your basic safety helm.

Available Commands:
    helm rawon fetch GITURL       Install a bare Helm rawon from Github (e.g git clone)
    helm rawon list               List installed Helm rawons
    helm rawon update NAME        Refresh an installed Helm rawon
    helm rawon delete NAME        Delete an installed Helm rawon
    helm rawon inspect NAME       Print out a rawon's readme
    helm rawon generate
    
Available Flags:
    --base-image, -bi             (Required when use generate command)
    --template, -t                (Required when use generate command)
    --help

Example Usage:
    asd
    abc
EOF
}

PASSTHRU=()
while [[ $# -gt 0 ]]
do
key="$1"

case $key in
    -bi|--base-image)
    BASE_IMAGE="$2"
    shift # past argument
    shift # past value
    ;;
    -t|--template)
    TEMPLATE="$2"
    shift # past argument
    shift # past value
    ;;
    --help)
    HELP=TRUE
    shift # past argument
    ;;
    *)
    PASSTHRU+=("$1") # save it in an array for later
    shift # past argument
    ;;
esac
done

# Restore PASSTHRU parameters
set -- "${PASSTHRU[@]}" 

# Show help if flagged
if [ "$HELP" == "TRUE" ]; then
    usage
    exit 0
fi

# Debug section
if [ "$DEBUG" == "TRUE" ]; then
    echo "PARAMS";
    echo $*;
    echo " ......................... ";
    echo Helm home      = "$HELM_HOME";
    echo REPO           = "${REPO}";
    echo BRANCH         = "${BRANCH}";
    echo CHARTPATH      = "${CHARTPATH}";
    echo PASSTHRU       = "${PASSTHRU[@]}";
    echo REPO_BASE_NAME = "$REPO_BASE_NAME";
    echo REPO_LOCATION  = "$REPO_LOCATION";
fi

# COMMAND must be either 'fetch', 'list', or 'delete'
COMMAND=${PASSTHRU[0]}

# Create the /starters directory, if needed
mkdir -p ${HELM_DATA_HOME}/starters

if [ "$COMMAND" == "fetch" ]; then
    REPO=${PASSTHRU[1]}
    cd ${HELM_DATA_HOME}/starters
    git clone ${REPO} --quiet
    exit 0
elif [ "$COMMAND" == "update" ]; then
    STARTER=${PASSTHRU[1]}
    cd ${HELM_DATA_HOME}/starters/${STARTER}
    git pull origin $(git rev-parse --abbrev-ref HEAD) --quiet
    exit 0
elif [ "$COMMAND" == "list" ]; then
    ls -A1 ${HELM_DATA_HOME}/starters
    exit 0
elif [ "$COMMAND" == "delete" ]; then 
    STARTER=${PASSTHRU[1]}
    rm -rf ${HELM_DATA_HOME}/starters/${STARTER}
    exit 0
elif [ "$COMMAND" == "inspect" ]; then 
    STARTER=${PASSTHRU[1]}
    find ${HELM_DATA_HOME}/starters/${STARTER} -type f -iname "*readme*" -exec cat {} \;
    exit 0
elif [ "$COMMAND" == "generate" ]; then
     # Base url Image
     BASE=docker.io/$BASE_IMAGE

     # Create and replace base image
     $HELM_BIN create ./manifest/"${PASSTHRU[@]:1}" --starter "$TEMPLATE"
     find ./manifest/"${PASSTHRU[@]:1}" -type f -name 'values*.*yaml' -exec sed -i \
        "s|<REPOSITORY>|$BASE|g" {} \;
    exit 0
else
    echo "Error: Invalid command."
    usage
    exit 1
fi
